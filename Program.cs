﻿using System;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create tree
            var tree = new BinaryTree(5);

            tree.AddToTree(8);
            tree.AddToTree(3);
            tree.AddToTree(10);
            tree.AddToTree(30);
            tree.AddToTree(40);
            tree.AddToTree(80);
            tree.AddToTree(50);
            tree.AddToTree(5);  // gets ignored
            tree.AddToTree(20);
            tree.AddToTree(15);

            tree.WriteOut();

            // Create balanced version
            var balancer = new TreeBalancer();
            var newTree = balancer.BalanceTree(tree);

            newTree.WriteOut();

            // Testing retrieving on non balanced and balanced tree
            Console.WriteLine("Retrieving in old tree");
            tree.Retrieve(50);
            Console.WriteLine("Retrieving in new tree");
            newTree.Retrieve(50);

            // try to retrieve something that isnt there
            newTree.Retrieve(7);

            Console.ReadLine();
        }
    }
}
