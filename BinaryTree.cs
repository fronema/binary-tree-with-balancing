﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    public class BinaryTree
    {
        public Node TopNode;

        public BinaryTree(int value = 0)
        {
            TopNode = new Node(value);
        }

        public void AddToTree(int value)
        {
            AddToTree(value, TopNode);
        }

        public void WriteOut()
        {
            var currentLine = new List<Node> { TopNode };
            WriteOutNextLevel(currentLine);
        }

        //Finds node with given value or returns null.
        public Node Retrieve(int value)
        {
            var node = RetrieveNode(TopNode, value);

            if (node != null)
            {
                Console.WriteLine($"I have found node with value {value}");
            }
            else
            {
                Console.WriteLine($"There is no node with value {value}");
            }

            return node;
        }

        // looks for node with given value. Recursive
        private Node RetrieveNode(Node node, int value)
        {
            if (node == null)
            {
                Console.WriteLine($"checking Node with no value");
                return null;
            }

            Console.WriteLine($"checking Node with value {node.Value}");
            if (node.Value == value)
            {
                return node;
            }
            else if (value < node.Value)
            {
                return RetrieveNode(node.LeftChildren, value);
            }
            else
            {
                return RetrieveNode(node.RightChildren, value);
            }

        }

        // Writes out whole tree in semi formated matter
        private static void WriteOutNextLevel(List<Node> currentLine)
        {
            var stringBuild = new StringBuilder();
            var nextLevel = new List<Node>();
            foreach (var curr in currentLine)
            {
                if (curr != null)
                {
                    stringBuild.Append(" " + curr.Value + " ");
                    nextLevel.Add(curr.LeftChildren);
                    nextLevel.Add(curr.RightChildren);
                }
                else
                {
                    stringBuild.Append(" x ");

                    nextLevel.Add(null);
                    nextLevel.Add(null);
                }

            }
            Console.WriteLine(stringBuild.ToString());

            if (nextLevel.Exists(x => x != null))   // Stop it there are no values in next level
            {
                WriteOutNextLevel(nextLevel);
            }
        }

        // adds new value to tree. Recursive
        private void AddToTree(int value, Node currentNode)
        {
            if (value > currentNode.Value) // put it on right side
            {
                if (currentNode.RightChildren == null)
                {
                    currentNode.RightChildren = new Node(value);
                }
                else
                {
                    AddToTree(value, currentNode.RightChildren);
                }
            }
            else if (value < currentNode.Value)
            {
                if (currentNode.LeftChildren == null)
                {
                    currentNode.LeftChildren = new Node(value);
                }
                else
                {
                    AddToTree(value, currentNode.LeftChildren);
                }            
            }
        }
    }
}
