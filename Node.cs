﻿namespace BinaryTree
{
    public class Node
    {
        public int Value;
        public Node LeftChildren;
        public Node RightChildren;

        public Node(int value)
        {
            Value = value;
        }

    }
}
