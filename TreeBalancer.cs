﻿using System.Collections.Generic;
using System.Linq;

namespace BinaryTree
{
    public class TreeBalancer
    {

        // make the tree as short in depth as possible
        public BinaryTree BalanceTree(BinaryTree tree) 
        {
            var values = GetAllValues(tree.TopNode, new List<int>());

          //  Console.WriteLine(string.Join(',', values));

            var balancedTree = CreateBalancedTree(values);

            return balancedTree;
        }


        private List<int> GetAllValues(Node node, List<int> values )
        {
            if (node != null)
            {
                values.Add(node.Value);
                values = GetAllValues(node.LeftChildren, values);
                values = GetAllValues(node.RightChildren, values);
            }
            return values;
        }

        // could be called directly if needed
        private BinaryTree CreateBalancedTree(List<int> values)
        {
            values.Sort();

            var tree = new BinaryTree
            {
                TopNode = GetBalancedNode(values)
            };

            return tree;
        }

        // Create balanced structure of nodes from all given values. Recursive
        private Node GetBalancedNode(List<int> values)
        {
            var indexOfMiddle = values.Count / 2;

            var middle = values[indexOfMiddle];
            var node = new Node(middle);
            var leftValues = values.Take(indexOfMiddle).ToList();    // take first half
            var rightValues = values.Skip(indexOfMiddle + 1).Take(indexOfMiddle).ToList();  // take second half
            if (leftValues.Count > 0)
            {
                node.LeftChildren = GetBalancedNode(leftValues);
            }
            if (rightValues.Count > 0)
            {
                node.RightChildren = GetBalancedNode(rightValues);
            }

            return node;
        }

    }
}
